package biblioteca_2;


public class Biblioteca {
	
    private String nombre;

    private int cantidad;

    /**
     * Default constructor
     */
    public Biblioteca() {
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

}