package biblioteca_2;

/**
 * 
 */
public class Persona {
	
    private String nombre;
    private String apellido;
    private String sexo;
    private String tipo;
    
    public Persona() {
    }
    public Persona(String nombre, String apellido, String sexo, String tipo) {
    		this.nombre =nombre;
    		this.apellido = apellido;
    		this.sexo = sexo;
    		this.tipo = tipo;
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


}