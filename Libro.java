package biblioteca_2;

public class Libro {
	   
    private String nombre;
    private String editorial;
    private int cantidad;
    private int id;

    public Libro() {
    }

    public void PrestarLibro() {
        // TODO implement here
    }


    public void DevolverLibro() {
        // TODO implement here
    }

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


}