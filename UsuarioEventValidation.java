package biblioteca_2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JOptionPane;

public class UsuarioEventValidation implements ActionListener {
	UsuarioEventValidation vm = this;
	UsuarioView ap;
	Usuario user;
	static boolean isEdit;
	static int actual = 0;
	
	public UsuarioEventValidation(UsuarioView usuarioView) {
		// TODO Auto-generated constructor stub
		ap = usuarioView;
		
	}
	
	private void showMessage(String message){
		JOptionPane.showMessageDialog (ap, message);
	}
	
	public boolean validateInformacion(){

		if(ap.t.getText().equals("") ||
		ap.t2.getText().equals("") ||
		ap.t3.getText().equals("") ||
		ap.t4.getText().equals("") || 
		ap.c.getSelectedItem().equals("")){
			
			vm.showMessage("Todos los campos son abligatorios, por favor revise nuevamente");
		    return false;	
		}
		else if (!ap.t.getText().matches("[a-zA-Z ]*")){
			vm.showMessage("El campo NOMBRE solo puede tener letras");
			return false;
		}
		return true;
	}
	
	public void saveInformation(){
		
		vm.showMessage("Usuario guardado exitosamente");
		
		ap.habilarCampos(false);
		
		System.out.println(vm.isEdit);
		
		if(vm.isEdit){
			System.out.println(actual);
			System.out.println(getUserFromTags());
			
			ap.usuarios.set(actual,getUserFromTags());
		}else{
			ap.usuarios.add(getUserFromTags());
		}
		
		
		vm.clearInformacion();
	}
	
	public Usuario getUserFromTags(){
		Usuario usr = new Usuario(ap.t4.getText(), ap.t.getText(),ap.t2.getText(),ap.t3.getText(),ap.c.getSelectedItem() );
		return usr;
	}
	
	
	public void nuevaInformacion(){
		vm.clearInformacion();
	    ap.habilarCampos(true);
	}
	
	private void clearInformacion(){
		ap.t.setText("");
		ap.t2.setText("");
		ap.t3.setText("");
		ap.t4.setText("");
		ap.c.select(0);
		vm.actual = 0;
		vm.isEdit = false;
	}
	
	
	private void viewInformation(Usuario user){
		
		ap.t.setText(user.getNombre());
		ap.t2.setText(user.getApellido());
		ap.t3.setText(user.getSexo());
		ap.t4.setText(user.getCarnet());
		
		for (int i = 0; i < ap.list.size(); i++) {
			if(ap.list.get(i) == user.getTipo()){
				ap.c.select(i);
				break;
			}
		}
		
		
	}

	
	public void editarInformacion(){
		if(!ap.isHab && !ap.t.getText().isEmpty() ){
			vm.isEdit = true;
			ap.habilarCampos(true);
		} 
	}
	
	public void borrarInformacion(){
		
		System.out.println(ap.isHab);
		if(!ap.t.getText().isEmpty() && !ap.isHab && ap.usuarios.size() > 0){
			ap.usuarios.remove(actual);
			if(actual > 0){
				actual--;	
			}
			vm.showMessage("Usuario elminado");
			vm.viewUserPosition(actual);
		}else{
			vm.showMessage("No es posible borrar");
		}
//		
	}
	
	public void  viewUserPosition(int postion){
		
		if(!ap.usuarios.isEmpty()){
			user = (Usuario) ap.usuarios.get(postion);
			vm.viewInformation(user);
		}else{
			vm.clearInformacion();
		}
		
		
	}

	public boolean getMovimiento(String opcion) {
		
		if(ap.usuarios.size() == 0) {
			showMessage("No existe ningun usuario");
			return false;
		}

		if (opcion == "<<")
			actual = 0;

		if (opcion == ">>")
			actual = ap.usuarios.size() - 1;

		if (opcion == "<") {
			if (actual > 0)
				actual--;
			else
				showMessage("Primer estudiante de la lista");
		}
		if (opcion == ">") {

			if (ap.usuarios.size() - 1 == actual)
				showMessage("Ultimo estudiante de la lista");
			else
				actual++;
		}
		return true;

	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		

		if(e.getSource() == ap.bsave){
			if(vm.validateInformacion()){
				vm.saveInformation();
			}
		}else if(e.getSource() == ap.badd){
			vm.nuevaInformacion();
		}else if(e.getSource() == ap.bdel){
			vm.borrarInformacion();
		}else if(e.getSource() == ap.bedit){
			vm.editarInformacion();
		}else{
			 ap.habilarCampos(false);
			 vm.isEdit = false;
			if(vm.getMovimiento(((JButton) e.getSource()).getText())){
				viewUserPosition(actual);
			}			
		}
			
		
	}
	
	
	
}
