package biblioteca_2;

public class Usuario extends Persona {

	private String carnet;
    public Usuario() {
    }
    
    public Usuario(String carnet,String nombre, String apellido, String sexo, String tipo) {
    	super(nombre ,apellido, sexo, tipo);  
    	this.carnet = carnet;
    }
       
    
	public String getCarnet() {
		return carnet;
	}
	public void setCarnet(String carnet) {
		
		this.carnet = carnet;
		
	}


}
