package biblioteca_2;

/**
 * 
 */
public class Peticion {
	
	private String fecha_salida;
    private String fecha_entrada;
    private String tipo_prestamo;
    private int tiempo_prestamo;

    /**
     * Default constructor
     */
    public Peticion() {
    }


    public void EntregarLibro() {
        // TODO implement here
    }

    public void SoliciarLibro() {
        // TODO implement here
    }


	public String getFecha_entrada() {
		return fecha_entrada;
	}


	public void setFecha_entrada(String fecha_entrada) {
		this.fecha_entrada = fecha_entrada;
	}


	public String getTipo_prestamo() {
		return tipo_prestamo;
	}


	public void setTipo_prestamo(String tipo_prestamo) {
		this.tipo_prestamo = tipo_prestamo;
	}


	public int getTiempo_prestamo() {
		return tiempo_prestamo;
	}


	public void setTiempo_prestamo(int tiempo_prestamo) {
		this.tiempo_prestamo = tiempo_prestamo;
	}


	public String getFecha_salida() {
		return fecha_salida;
	}


	public void setFecha_salida(String fecha_salida) {
		this.fecha_salida = fecha_salida;
	}


}